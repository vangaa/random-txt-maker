# Random test file maker
---

Este programa crea un archivo con una matriz de números aleatorios en su 
interior. Esta pensado especialmente para testear aquellos programas que 
necesiten leer los datos de entrada de un archivo, el cual si es muy pequeño 
no se alcanza a notar ni el tiempo ni el comportamiento del programa con 
archivos de entrada muy grandes.

Los parámetros de creación del archivo son especificados por parámetro.

## Ejecución

El programa necesita una serie de parámetros para crear el archivo de testeo, 
los cuales son:

 - **l** - El numero de lineas de la matriz o altura
 - **c** - El numero de columnas del la matriz o ancho.
 - **i** - Es el menor numero que pueden tomar los números aleatorios
 - **s** - Es el mayor numero que pueden tomar los números aleatorios
 
Especificar un nombre de salida es opcional. Si no lo hace, se crea la matriz 
en un archivo por defecto llamado 'random.txt'.

Algunos ejemplos:

 - Se crea un archivo de nombre 'prueba\_1.dat' de 100 lineas, 50 columnas y 
   con un rango de numeros desde el 0 hasta el 10.  
   `rand-txt-mk prueba_1.dat -l 100 -c 50 -i 0 -s 10`
 - Se crea un archivo por defecto de 10 lineas y 100 columnas, con numeros en 
   el rango de -10 hasta 5.  
  `rand-txt-mk -l 10 -c 100 -i -10 -s 5`

## Compilación

Ejecute simplemente `make` y se compilará automáticamente.
