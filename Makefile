CC = gcc
CFLAGS = -Wall -g
PROGNAME = rand-txt-mk

all: $(PROGNAME)

$(PROGNAME): randtxtmk.h

$(PROGNAME): main.c randtxtmk.c
	$(CC) $(CFLAGS) -o $(PROGNAME) $^
