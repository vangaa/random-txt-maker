#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "randtxtmk.h"

void make_random_txt(int lineas, int columnas,
		int inferior, int superior, char filename[])
{
	FILE * file;
	int i, j;
	unsigned int semilla = time(NULL);
	superior = superior - inferior+1;
	file = fopen(filename, "w");
	srand(semilla);

	for (i=0; i<lineas; i++)
	{
		for (j=0; j<columnas; j++)
		{
			fprintf(file, "%i", rand()%superior+inferior);

			if (j!=columnas-1)
				fprintf(file, "%c", ' ');
		}
		fprintf(file, "%c", '\n');
	}

	fclose(file);
}
