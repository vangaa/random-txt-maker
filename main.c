#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "randtxtmk.h"

const char usage[] =
			"opciones ... [archivo-salida]\n"
			"\n"
			"opciones:\n"
			"  -l <num-lineas>\n"
			"     Numero de lineas del archivo\n"
			"  -c <num-columnas>\n"
			"     Numero de columnas\n"
			"  -s <lim-superior>\n"
			"     Es el numero maximo superior que toman los numeros\n"
			"  -i <lim-inferior>\n"
			"     Es el limite inferior que toman los numeros\n";

int main (int argc, char** argv)
{
	int lineas, columnas;
	int lim_sup, lim_inf;
	int opt;
	char* filename = "random.txt";

	if (argc == 1)
	{
		printf("uso: %s %s", argv[0], usage);
		exit(EXIT_FAILURE);
	}

	lineas = columnas = 0;

	while ((opt = getopt(argc, argv, ":l:c:s:i:")) != -1)
	{
		switch (opt)
		{
			case 'l':
				sscanf(optarg, "%d", &lineas);
				if (lineas <= 0) goto error;
				break;

			case 'c':
				sscanf(optarg, "%d", &columnas);
				if (columnas <= 0) goto error;
				break;

			case 's':
				sscanf(optarg, "%d", &lim_sup);
				break;

			case 'i':
				sscanf(optarg, "%d", &lim_inf);
				break;

			case ':': /* missing argument */
				fprintf(stderr, "La opcion `%c' necesita un argumento\n", optopt);
				exit(EXIT_FAILURE);

			default: /* unknow option (?)*/
				fprintf(stderr, "Opcion `%c' desconocida\n", optopt);
				exit(EXIT_FAILURE);
		}
	}
	goto make;

	error:
	fprintf(stderr, "Mal argumento en la opcion `%c'\n", opt);
	exit(EXIT_FAILURE);

	make:



	if (lim_sup < lim_inf)
	{
		fprintf(stderr, "Los limites no son coherentes\n");
		exit(EXIT_FAILURE);
	}
	else if (argc < 9)
	{
		fprintf(stderr, "Faltan argumentos\n");
	}
	else if (argc > 9)
		switch (strcmp(argv[0], argv[optind]))
		{
			case 0: // iguales
				fprintf(stderr, "Nombre de salida invalido: `%s'", argv[optind]);
				exit(EXIT_FAILURE);
			default:
				filename = argv[optind];
		}

	make_random_txt(lineas, columnas, lim_inf, lim_sup, filename);

	exit(EXIT_SUCCESS);
}
